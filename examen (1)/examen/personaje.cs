using System;
class Personaje
{
    protected string Nombre;

    public Personaje(string nombre)
    {
        this.Nombre = nombre;
    }

    public virtual string ObtenerNombre()
    {
        return Nombre;
    }
}