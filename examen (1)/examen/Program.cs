﻿
class Program
{
    static void Main()
    {

        Personaje goku = new goku();
        Personaje vegeta = new vegeta();
        Personaje gohan = new gohan();
        Personaje trunks = new trunks();


        Fusiones gogeta = new Gogeta(goku, vegeta);
        Fusiones gotenks = new Gotenks(gohan, trunks);

        Console.WriteLine("------------------------------------------------------------------------");
        Console.WriteLine(goku.ObtenerNombre());
        Console.WriteLine(vegeta.ObtenerNombre());
        Console.WriteLine("------------------------------------------------------------------------");
        Console.WriteLine(gohan.ObtenerNombre());
        Console.WriteLine(trunks.ObtenerNombre());
        Console.WriteLine("------------------------------------------------------------------------");
        Console.WriteLine("nombres de las funciones resultantes");
        Console.WriteLine(gogeta.ObtenerNombre());
        Console.WriteLine(gotenks.ObtenerNombre());

    }
}
